import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { NumericFormatingAndValidationDirective } from "./derectives/numeric-format-validation.directive";
import { ClickOutsideDirective } from "./derectives/click-outside.directive";

import { InfoTextFieldComponent } from "./slices/info-text-field/info-text-field.component";
import { CurrencyPickerComponent } from "./slices/currency-picker/currency-picker.component";
import { PeriodPickerComponent } from "./slices/period-picker/period-picker.component";
import { AprPercentComponent } from "./slices/apr-percent/apr-percent.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { NavButtonComponent } from "./slices/nav-button/nav-button.component";
import { TextfieldComponent } from "./slices/textfield/textfield.component";
import { ToggleComponent } from "./slices/toggle/toggle.component";
import { AppComponent } from "./app.component";

@NgModule({
  declarations: [
    NumericFormatingAndValidationDirective,
    ClickOutsideDirective,

    InfoTextFieldComponent,
    CurrencyPickerComponent,
    PeriodPickerComponent,
    DashboardComponent,
    NavButtonComponent,
    AprPercentComponent,
    TextfieldComponent,
    ToggleComponent,
    AppComponent,
  ],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
