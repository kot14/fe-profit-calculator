import { Component } from "@angular/core";
import { Subscription } from "rxjs";

import { settings } from "src/assets/mock-data";
import {
  ICurrency,
  IProfit,
  StateService,
} from "src/app/services/state.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent {
  public navList: { name: string; active: boolean }[] = [
    { name: "Lorem1", active: true },
    { name: "Lorem2", active: false },
    { name: "Lorem3", active: false },
    { name: "Lorem4", active: false },
  ];

  public currency?: ICurrency;
  public amount: number = 0;
  public formatedAmount: string = "";
  public profit?: IProfit;
  public period?: number;
  public isAmountValidated: string = "";

  public subscriptions: Subscription[] = [];
  constructor(public state: StateService) {
    this.subscriptions.push(
      state.currentCurrency.subscribe((res) => (this.currency = res)),
      state.currentAmount.subscribe((res) => {
        this.amount = res;
        this.formatedAmount = res
          .toString()
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }),
      state.profit.subscribe((res) => (this.profit = res)),
      state.currentPeriod.subscribe((res) => (this.period = res))
    );
  }

  ngOnInit() {
    this.state.recalculateProfit();
  }

  changeControlsTab(navIndex: number): void {
    this.navList = this.navList.map((nav, i) => {
      return { ...nav, active: i === navIndex };
    });
  }

  manualAmountEntry(value: string | null): void {
    if (value) {
      const amount = +value?.replaceAll(",", "");
      if (amount && this.validateAmount(amount)) {
        this.state.currentAmount.next(amount);
        this.state.recalculateProfit();
      }
    }
  }

  changeAmount(action: boolean): void {
    if (action) {
      if (this.amount < settings.maxAmount)
        this.state.currentAmount.next(this.amount + settings.amountStep);
    } else {
      if (this.amount > settings.minAmount)
        this.state.currentAmount.next(this.amount - settings.amountStep);
    }

    if (this.validateAmount(this.amount)) {
      this.state.recalculateProfit();
    }
  }

  validateAmount(amount: number): boolean {
    if (amount >= settings.minAmount && amount <= settings.maxAmount) {
      this.isAmountValidated = "";
      return true;
    } else {
      if (amount > settings.maxAmount) {
        this.isAmountValidated = `The maximum value must be less than ${settings.maxAmount}`;
      }
      if (amount < settings.minAmount) {
        this.isAmountValidated = `The minimum value must be greater than ${settings.minAmount}`;
      }
    }
    return false;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subsc) => subsc.unsubscribe());
  }
}
