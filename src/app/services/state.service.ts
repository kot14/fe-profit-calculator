import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

import { avaliablePeriod, currencyList, settings } from "src/assets/mock-data";

export interface ICurrency {
  name: string;
  apr: number;
}
export interface IProfit {
  profit: string;
  inPercent: string;
}

@Injectable({
  providedIn: "root",
})
export class StateService {
  public currentPeriod: BehaviorSubject<number> = new BehaviorSubject<number>(
    avaliablePeriod[0]
  );

  public currentAmount: BehaviorSubject<number> = new BehaviorSubject<number>(
    settings.minAmount
  );

  public currentCurrency: BehaviorSubject<ICurrency> =
    new BehaviorSubject<ICurrency>(currencyList[0]);

  public profit: BehaviorSubject<IProfit> = new BehaviorSubject<IProfit>({
    profit: "0",
    inPercent: "0%",
  });

  constructor() {}

  changePeriod(value: number): void {
    this.currentPeriod.next(value);
    this.recalculateProfit();
  }

  changeCurrency(currency: ICurrency): void {
    this.currentCurrency.next(currency);
    this.recalculateProfit();
  }

  recalculateProfit(): void {
    const deposit = this.currentAmount.getValue(),
      profit = this.currentCurrency.getValue().apr / 100,
      term = this.currentPeriod.getValue() / 12;

    const calculatedProfit = this.calculate(deposit, profit, term);
    const calculatedProfitInPercent: string = `${
      (calculatedProfit - deposit) / 100
    }%`;
    let formatedValue = (calculatedProfit - deposit)
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    this.profit.next({
      profit: formatedValue,
      inPercent: calculatedProfitInPercent,
    });
  }

  calculate(initial: number, interest: number, term: number): number {
    return Math.round(initial * (1 + interest * term));
  }
}
