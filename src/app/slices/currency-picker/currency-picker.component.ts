import { Component, EventEmitter, Input, Output } from "@angular/core";

import { ICurrency, StateService } from "src/app/services/state.service";
import { currencyList } from "src/assets/mock-data";

@Component({
  selector: "app-currency-picker",
  templateUrl: "./currency-picker.component.html",
  styleUrls: ["./currency-picker.component.scss"],
})
export class CurrencyPickerComponent {
  public currencyList = currencyList;

  @Input() currentCurency?: { name: string | number; apr: number };

  @Output() close: EventEmitter<Event> = new EventEmitter<Event>();
  constructor(public state: StateService) {}

  searchCurrency(searched: string): void {
    if (searched.length > 0) {
      this.currencyList = currencyList.filter((curr) =>
        curr.name.toLocaleLowerCase().includes(searched.toLocaleLowerCase())
      );
    } else {
      this.currencyList = currencyList;
    }
  }

  changeCurrency(currency: ICurrency): void {
    this.state.changeCurrency(currency);
    this.close.emit();
  }
}
