import { EventEmitter, Component, Input, Output } from "@angular/core";
import { settings } from "src/assets/mock-data";

@Component({
  selector: "app-textfield",
  templateUrl: "./textfield.component.html",
  styleUrls: ["./textfield.component.scss"],
})
export class TextfieldComponent {
  @Input() props:
    | {
        controls?: boolean;
        dropdown?: boolean;
        label: string;
        editable?: boolean;
        width?: string;
      }
    | undefined;
  @Input() value?: string | number;
  @Input() percent?: number;
  @Input() invalided?: string = "";

  @Output() changeAmount: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() changeInput: EventEmitter<string> = new EventEmitter<string>();

  public showDropdown: boolean = false;
  constructor() {}
}
