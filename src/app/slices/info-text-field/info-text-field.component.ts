import { Component, Input } from "@angular/core";

@Component({
  selector: "app-info-text-field",
  templateUrl: "./info-text-field.component.html",
  styleUrls: ["./info-text-field.component.scss"],
})
export class InfoTextFieldComponent {
  @Input() props: { text: string; tooltipText: string } | undefined;
}
