import { Component, EventEmitter, Input, Output } from "@angular/core";

import { StateService } from "src/app/services/state.service";
import { avaliablePeriod } from "src/assets/mock-data";

@Component({
  selector: "app-period-picker",
  templateUrl: "./period-picker.component.html",
  styleUrls: ["./period-picker.component.scss"],
})
export class PeriodPickerComponent {
  @Input() currentPeriod?: string | number = 0;

  @Output() close: EventEmitter<Event> = new EventEmitter<Event>();
  public periods = avaliablePeriod;

  constructor(public state: StateService) {}
  changePeriod(period: number): void {
    this.state.changePeriod(period);
    this.close.emit();
  }
}
