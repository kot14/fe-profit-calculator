import { Directive, ElementRef, HostListener } from "@angular/core";

@Directive({
  selector: "input[formatAndValidate]",
})
export class NumericFormatingAndValidationDirective {
  constructor(private _inputEl: ElementRef) {}

  @HostListener("input", ["$event"])
  onInput(event: KeyboardEvent) {
    if (this._inputEl.nativeElement.value === "-") return;
    let commasAndDotRemoved = this._inputEl.nativeElement.value.replace(
      /[.,\s]/g,
      ""
    );
    let toInt: number;
    let toLocale: string;
    if (commasAndDotRemoved.split(".").length > 1) {
      let decimal = isNaN(parseInt(commasAndDotRemoved.split(".")[1]))
        ? ""
        : parseInt(commasAndDotRemoved.split(".")[1]);
      toInt = parseInt(commasAndDotRemoved);
      toLocale = toInt.toLocaleString("en-US") + "." + decimal;
    } else {
      toInt = parseInt(commasAndDotRemoved);
      toLocale = toInt.toLocaleString("en-US");
    }
    if (toLocale === "NaN") {
      this._inputEl.nativeElement.value = "";
    } else {
      this._inputEl.nativeElement.value = toLocale;
    }
  }
}
