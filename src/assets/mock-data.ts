import { ICurrency } from "src/app/services/state.service";

export const avaliablePeriod: number[] = [1, 3, 6, 12, 24];

export const settings: {
  amountStep: number;
  maxAmount: number;
  minAmount: number;
} = { amountStep: 1000, maxAmount: 1000000, minAmount: 1000 };

export const currencyList: ICurrency[] = [
  { name: "TUSD (Test US Dollar)", apr: 12 },
  { name: "TEUR (Test Euro)", apr: 13 },
  { name: "TCNY (Test Chinese Yuan)", apr: 20 },
  { name: "TINR (Test Indian Rupee)", apr: 33 },
  { name: "TBRL (Test Brazilian Real)", apr: 21 },
  { name: "TIDR (Test Indonesian Rupiah)", apr: 80 },
];
